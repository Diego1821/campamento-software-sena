<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;


class StoreBootcampRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "name" => "required",
            "description"=>"required",
            "website" => "required|URL",
            "phone"=>"required",
            "user_id"=>"required|exists:users,id",
            "average_rating"=>"required",
            "average_cost"=>"required"
        ];
    }

    //metodo para enviar respuesta d eerror
    protected function failedValidation(Validator $v){

          throw new HttpResponseException( 
            response()->json(["sucess"=>false, "errors" => $v->errors()], 422)
        );

        
    }
}
