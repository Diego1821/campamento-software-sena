<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bootcamp;
use App\Http\Request\StoreBootcampRequest;
Use App\Http\Resources\BootcampResource; 
Use App\Http\Resources\BootcampCollection; 
use App\Http\Controllers\BaseController;

class BootcampController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //metodo json:
        //parametros: 1. data a enviar al cliente 
                    //2. codigo status http
        // return response()->json( new BootcampCollection(Bootcamp::all()) ,200)
        try{

            return $this->sendResponse(new BootcampCollection(Bootcamp::all()));    

        }catch(\Exception $e){

            return $this->sendError('server error', 500);

        }

           

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBootcampRequest $request)
    {
        
        try {
            return $this->sendResponse(new BootcampResource(Bootcamp::create($request->all())),201);

        } catch (\Exception $e) {

            return $this->sendError('server error', 500);
        }
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        try{
            $bootcamp = Bootcamp::find($id);
            //2.en caso que el bootcamp no exista 
            if(!$bootcamp){
                return $this->sendError("bootcamp with id: $id not font", 400);

            }
            return $this->sendResponse(new BootcampResource($bootcamp));
        }catch(\Exception $e){

            return $this->sendError('server error', 500);
        }
       //metodo json:
        //parametros: 1. data a enviar al cliente 
                    //2. codigo status http

        //1.encontrar el bootcamp por id
       
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            //1.Localozar el bootcamp con el id
        $b = Bootcamp::find($id);
        if(!$b){
            return $this->sendError("bootcamp with id: $id not font", 400);
        }
        //2.actualizarlo con un update
        $b->update($request->all());
        return $this->sendResponse(new BootcampResource($b));

        } catch (\Exception $e) {

            return $this->sendError('server error', 500);
        }
        
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
                //1.Localozar el bootcamp con el id
        $b = Bootcamp::find($id);
        if(!$b){
            return $this->sendError("bootcamp with id: $id not font", 400);
        }
        //2.actualizarlo con un update
        $b->delete();
        return $this->sendResponse(new BootcampResource($b));
        } catch (\Exception $e) {
            return $this->sendError('server error', 500);
        }
    
    }
}
